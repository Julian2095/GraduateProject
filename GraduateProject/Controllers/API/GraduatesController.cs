﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GraduateProject.Models;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using GraduateProject.Classes;

namespace GraduateProject.Controllers.API
{
    [RoutePrefix("api/Graduates")]
    public class GraduatesController : ApiController
    {
        private GraduateProjectContext db = new GraduateProjectContext();

        [HttpPost]
        [Route("Login")]
        public IHttpActionResult Login(JObject form)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var email = string.Empty;
            var password = string.Empty;
            dynamic jsonObject = form;

            try
            {
                email = jsonObject.Email.Value;
                password = jsonObject.Password.Value;
            }
            catch
            {
                return BadRequest("Incorrect call");
            }
            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = userManager.Find(email, password);

            if(userASP == null)
            {
                return BadRequest("wrong user or password");
            }
            var graduate = db.Graduates.Where(u => u.GraduateName == email)
                .Include(g => g.City)
                .Include(g => g.Department).FirstOrDefault();

            if (graduate == null)
            {
                return BadRequest("wrong user or password");
            }

            var graduateResponse = new GraduateResponse
            {
                Address = graduate.Adress,
                CityId = graduate.CityId,
                CityName = graduate.City.Name,
                DepartmentId = graduate.DepartmentId,
                DepartmentName = graduate.Department.Name,
                FirstName = graduate.FirstName,
                LastName = graduate.LastName,
                Phone = graduate.Phone,
                Photo = graduate.Photo,
                GraduateId = graduate.GraduateId,
                GraduateName = graduate.GraduateName, 
            };
            return Ok(graduateResponse);
        }

        // GET: api/Graduates
        public IQueryable<Graduate> GetGraduates()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Graduates;
        }

        // GET: api/Graduates/5
        [ResponseType(typeof(Graduate))]
        public IHttpActionResult GetGraduate(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Graduate graduate = db.Graduates.Find(id);
            if (graduate == null)
            {
                return NotFound();
            }

            return Ok(graduate);
        }

        // PUT: api/Graduates/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGraduate(int id, Graduate graduate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != graduate.GraduateId)
            {
                return BadRequest();
            }

            db.Entry(graduate).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GraduateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Graduates
        [ResponseType(typeof(Graduate))]
        public IHttpActionResult PostGraduate(Graduate graduate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Graduates.Add(graduate);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = graduate.GraduateId }, graduate);
        }

        // DELETE: api/Graduates/5
        [ResponseType(typeof(Graduate))]
        public IHttpActionResult DeleteGraduate(int id)
        {
            Graduate graduate = db.Graduates.Find(id);
            if (graduate == null)
            {
                return NotFound();
            }

            db.Graduates.Remove(graduate);
            db.SaveChanges();

            return Ok(graduate);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GraduateExists(int id)
        {
            return db.Graduates.Count(e => e.GraduateId == id) > 0;
        }
    }
}