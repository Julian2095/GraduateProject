﻿using GraduateProject.Models;
using PagedList;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace GraduateProject.Controllers
{
    public class OpinionsController : Controller
    {
        private GraduateProjectContext db = new GraduateProjectContext();

        // GET: 

        [Authorize(Roles = "Graduate, Admin")]
        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            var opinions = db.Opinions.Include(o => o.Graduate);
            return View(opinions.OrderBy(g=>g.Message).ToPagedList((int)page, 5));
        }

        // GET: Opinions/Details/5
        [Authorize(Roles = "Graduate, Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opinion opinion = db.Opinions.Find(id);
            if (opinion == null)
            {
                return HttpNotFound();
            }
            return View(opinion);
        }

        // GET: Opinions/Create
        [Authorize(Roles = "Graduate, Admin")]
        public ActionResult Create()
        {
            ViewBag.GraduateId = new SelectList(db.Graduates.Where(g => g.GraduateName == User.Identity.Name), "GraduateId", "GraduateName");
            return View();
        }

        // POST: Opinions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Opiniond,OpinionName,Message,GraduateId")] Opinion opinion)
        {
            if (ModelState.IsValid)
            {
                db.Opinions.Add(opinion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GraduateId = new SelectList(db.Graduates, "GraduateId", "GraduateName", opinion.GraduateId);
            return View(opinion);
        }

        // GET: Opinions/Edit/5
        [Authorize(Roles = "Graduate, Admin")]
        public ActionResult Edit(int? id)
        {
            try
            {
                var confirmacion = db.Graduates.Where(g => g.GraduateName == User.Identity.Name).FirstOrDefault();
                var ID = confirmacion.GraduateId;
                if (id != ID)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opinion opinion = db.Opinions.Find(id);
            if (opinion == null)
            {
                return HttpNotFound();
            }
            ViewBag.GraduateId = new SelectList(db.Graduates.Where(g => g.GraduateName == User.Identity.Name), "GraduateId", "GraduateName", opinion.GraduateId);
            return View(opinion);
        }

        // POST: Opinions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Opiniond,OpinionName,Message,GraduateId")] Opinion opinion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(opinion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GraduateId = new SelectList(db.Graduates, "GraduateId", "GraduateName", opinion.GraduateId);
            return View(opinion);
        }

        // GET: Opinions/Delete/5
        [Authorize(Roles = "Graduate, Admin")]
        public ActionResult Delete(int? id)
        {
            try
            {
                var confirmacion = db.Graduates.Where(g => g.GraduateName == User.Identity.Name).FirstOrDefault();
                var ID = confirmacion.GraduateId;
                if (id != ID)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opinion opinion = db.Opinions.Find(id);
            if (opinion == null)
            {
                return HttpNotFound();
            }
            return View(opinion);
        }

        // POST: Opinions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Opinion opinion = db.Opinions.Find(id);
            db.Opinions.Remove(opinion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
