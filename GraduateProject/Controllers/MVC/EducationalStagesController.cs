﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GraduateProject.Models;
using PagedList;

namespace GraduateProject.Controllers
{
    
    public class EducationalStagesController : Controller
    {
        private GraduateProjectContext db = new GraduateProjectContext();

        // GET: EducationalStages
        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            return View(db.EducationalStages.OrderBy(e => e.Name).ToPagedList((int)page, 5));
        }
        [Authorize(Roles = "Graduate, Admin")]
        // GET: EducationalStages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EducationalStage educationalStage = db.EducationalStages.Find(id);
            if (educationalStage == null)
            {
                return HttpNotFound();
            }
            return View(educationalStage);
        }
        [Authorize(Roles = "Admin")]
        // GET: EducationalStages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EducationalStages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EducationalStageId,Name")] EducationalStage educationalStage)
        {
            if (ModelState.IsValid)
            {
                db.EducationalStages.Add(educationalStage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(educationalStage);
        }
        [Authorize(Roles = "Admin")]
        // GET: EducationalStages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EducationalStage educationalStage = db.EducationalStages.Find(id);
            if (educationalStage == null)
            {
                return HttpNotFound();
            }
            return View(educationalStage);
        }

        // POST: EducationalStages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EducationalStageId,Name")] EducationalStage educationalStage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(educationalStage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(educationalStage);
        }
        [Authorize(Roles = "Admin")]
        // GET: EducationalStages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EducationalStage educationalStage = db.EducationalStages.Find(id);
            if (educationalStage == null)
            {
                return HttpNotFound();
            }
            return View(educationalStage);
        }

        // POST: EducationalStages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EducationalStage educationalStage = db.EducationalStages.Find(id);
            db.EducationalStages.Remove(educationalStage);
            try
            {
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                                  ex.InnerException.InnerException != null &&
                                  ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "Can't be delate, it has relations");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            return View(educationalStage);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
