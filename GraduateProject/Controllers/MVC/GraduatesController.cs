﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GraduateProject.Models;
using GraduateProject.Classes;
using PagedList;

namespace GraduateProject.Controllers
{
    public class GraduatesController : Controller
    {
        private GraduateProjectContext db = new GraduateProjectContext();

        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            var graduates = db.Graduates.Include(g => g.City).Include(g => g.Department).OrderBy(g => g.FirstName).ThenBy(u => u.City.Name);
            return View(graduates.ToPagedList((int)page, 5));
        }
        [Authorize(Roles = "Graduate, Admin")]
        // GET: Graduates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Graduate graduate = db.Graduates.Find(id);
            if (graduate == null)
            {
                return HttpNotFound();
            }
            return View(graduate);
        }
        [Authorize(Roles = "Admin")]
        // GET: Graduates/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(), "CityId", "Name");
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name");
            return View();
        }

        // POST: Graduates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create(Graduate graduate)
        {
            if (ModelState.IsValid)
            {
                db.Graduates.Add(graduate);
                db.SaveChanges();
                UserHelper.CreateUserASP(graduate.GraduateName, "Graduate");

                if (graduate.PhotoFile != null)
                {
                    var folder = "~/Content/Graduates";
                    var file = string.Format("{0}.jpg", graduate.GraduateId);
                    var response = FileHelper.UploadPhoto(graduate.PhotoFile, folder, file);
                    if (response)
                    {
                        var pic = string.Format("{0}/{1}", folder, file);
                        graduate.Photo = pic;
                        db.Entry(graduate).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }

            ViewBag.CityId = new SelectList(CombosHelper.GetCities(), "CityId", "Name", graduate.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", graduate.DepartmentId);
            return View(graduate);
        }
        //[Authorize(Roles = "Admin")]
        // GET: Graduates/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                var confirmacion = db.Graduates.Where(g => g.GraduateName == User.Identity.Name).FirstOrDefault();
                var ID = confirmacion.GraduateId;
                if (id != ID)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                    return RedirectToAction("Index");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var graduate = db.Graduates.Find(id);
            if (graduate == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(), "CityId", "Name", graduate.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", graduate.DepartmentId);
            return View(graduate);
        }
        // POST: Graduates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Graduate graduate)
        {
            if (ModelState.IsValid)
            {
                if (graduate.PhotoFile != null)
                {
                    var pic = string.Empty;
                    var folder = "~/Content/Graduates";
                    var file = string.Format("{0}.jpg", graduate.GraduateId);
                    var response = FileHelper.UploadPhoto(graduate.PhotoFile, folder, file);
                    if (response)
                    {
                        pic = string.Format("{0}/{1}", folder, file);
                        graduate.Photo = pic;
                    }
                }

                var db2 = new GraduateProjectContext();
                var currentUser = db2.Graduates.Find(graduate.GraduateId);
                if (currentUser.GraduateName != graduate.GraduateName)
                {
                    UserHelper.updateUserName(currentUser.GraduateName, graduate.GraduateName);
                }
                db2.Dispose();

                try
                {
                    db.Entry(graduate).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.CityId = new SelectList(CombosHelper.GetCities(), "CityId", "Name", graduate.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", graduate.DepartmentId);
            return View(graduate);
        }
        [Authorize(Roles = "Admin")]
        // GET: Graduates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Graduate graduate = db.Graduates.Find(id);
            if (graduate == null)
            {
                return HttpNotFound();
            }
            return View(graduate);
        }

        // POST: Graduates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Graduate graduate = db.Graduates.Find(id);
            db.Graduates.Remove(graduate);
            try
            {
                db.SaveChanges();
                UserHelper.DeleteUser(graduate.GraduateName);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                 ex.InnerException.InnerException != null &&
                 ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "Can't be delate, it has relations");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            return View(graduate);
        }

        public JsonResult GetCities(int departmentId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cities = db.Cities.Where(m => m.DepartmentId == departmentId);
            return Json(cities);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
