﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GraduateProject.Models;
using PagedList;

namespace GraduateProject.Controllers
{
    public class LaborSuppliesController : Controller
    {
        private GraduateProjectContext db = new GraduateProjectContext();

        [Authorize(Roles = "Graduate, Admin")]
        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            var laborSupplies = db.LaborSupplies.Include(l => l.City).Include(l => l.Company).Include(l => l.Department);
            return View(laborSupplies.OrderBy(o => o.LaborSupplyName).ThenBy(g => g.City.Name).ToPagedList((int) page, 5));
        }
        [Authorize(Roles = "Graduate, Admin")]
        // GET: LaborSupplies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LaborSupply laborSupply = db.LaborSupplies.Find(id);
            if (laborSupply == null)
            {
                return HttpNotFound();
            }
            return View(laborSupply);
        }
        [Authorize(Roles = "Admin")]
        // GET: LaborSupplies/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "Name");
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name");
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name");
            return View();
        }

        // POST: LaborSupplies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LaborSupplyId,LaborSupplyName,Description,Salary,CompanyId,DepartmentId,CityId")] LaborSupply laborSupply)
        {
            if (ModelState.IsValid)
            {
                db.LaborSupplies.Add(laborSupply);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CityId = new SelectList(db.Cities, "CityId", "Name", laborSupply.CityId);
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", laborSupply.CompanyId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", laborSupply.DepartmentId);
            return View(laborSupply);
        }
        [Authorize(Roles = "Admin")]
        // GET: LaborSupplies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LaborSupply laborSupply = db.LaborSupplies.Find(id);
            if (laborSupply == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "Name", laborSupply.CityId);
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", laborSupply.CompanyId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", laborSupply.DepartmentId);
            return View(laborSupply);
        }

        // POST: LaborSupplies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LaborSupplyId,LaborSupplyName,Description,Salary,CompanyId,DepartmentId,CityId")] LaborSupply laborSupply)
        {
            if (ModelState.IsValid)
            {
                db.Entry(laborSupply).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "Name", laborSupply.CityId);
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", laborSupply.CompanyId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", laborSupply.DepartmentId);
            return View(laborSupply);
        }
        [Authorize(Roles = "Admin")]
        // GET: LaborSupplies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LaborSupply laborSupply = db.LaborSupplies.Find(id);
            if (laborSupply == null)
            {
                return HttpNotFound();
            }
            return View(laborSupply);
        }

        // POST: LaborSupplies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LaborSupply laborSupply = db.LaborSupplies.Find(id);
            db.LaborSupplies.Remove(laborSupply);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
