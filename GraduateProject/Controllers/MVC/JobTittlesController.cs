﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GraduateProject.Models;
using PagedList;

namespace GraduateProject.Controllers
{
    public class JobTittlesController : Controller
    {
        private GraduateProjectContext db = new GraduateProjectContext();

        // GET: JobTittles
        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            return View(db.JobTittles.OrderBy(j =>j.JobTittleName).ToPagedList((int)page, 5));
        }

        [Authorize(Roles = "Graduate, Admin")]
        // GET: JobTittles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobTittle jobTittle = db.JobTittles.Find(id);
            if (jobTittle == null)
            {
                return HttpNotFound();
            }
            return View(jobTittle);
        }
        [Authorize(Roles = "Admin")]
        // GET: JobTittles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: JobTittles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "JobTittleId,JobTittleName")] JobTittle jobTittle)
        {
            if (ModelState.IsValid)
            {
                db.JobTittles.Add(jobTittle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(jobTittle);
        }
        [Authorize(Roles = "Admin")]
        // GET: JobTittles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobTittle jobTittle = db.JobTittles.Find(id);
            if (jobTittle == null)
            {
                return HttpNotFound();
            }
            return View(jobTittle);
        }

        // POST: JobTittles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "JobTittleId,JobTittleName")] JobTittle jobTittle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(jobTittle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(jobTittle);
        }
        [Authorize(Roles = "Admin")]
        // GET: JobTittles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobTittle jobTittle = db.JobTittles.Find(id);
            if (jobTittle == null)
            {
                return HttpNotFound();
            }
            return View(jobTittle);
        }

        // POST: JobTittles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            JobTittle jobTittle = db.JobTittles.Find(id);
            db.JobTittles.Remove(jobTittle);
            try
            {
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                  ex.InnerException.InnerException != null &&
                  ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "Can't be delate, it has relations");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            return View(jobTittle);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
