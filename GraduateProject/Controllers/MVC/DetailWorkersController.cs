﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GraduateProject.Models;
using GraduateProject.Classes;
using PagedList;

namespace GraduateProject.Controllers
{
    public class DetailWorkersController : Controller
    {
        private GraduateProjectContext db = new GraduateProjectContext();

        // GET: DetailWorkers
        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            var detailWorkers = db.DetailWorkers.Include(d => d.Company).Include(d => d.Graduate).Include(d => d.JobTittle)
                .OrderBy(c =>c.Company.Name).ThenBy(c => c.JobTittle.JobTittleName);
            return View(detailWorkers.ToPagedList((int)page, 5));
        }
        [Authorize(Roles = "Graduate, Admin")]
        // GET: DetailWorkers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetailWorker detailWorker = db.DetailWorkers.Find(id);
            if (detailWorker == null)
            {
                return HttpNotFound();
            }
            return View(detailWorker);
        }
        [Authorize(Roles = "Admin")]
        // GET: DetailWorkers/Create
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(CombosHelper.Getcompanies(), "CompanyId", "Name");
            ViewBag.GraduateId = new SelectList(CombosHelper.GetGraduates(), "GraduateId", "GraduateName");
            ViewBag.JobTittleId = new SelectList(CombosHelper.GetJobTittles(), "JobTittleId", "JobTittleName");
            return View();
        }

        // POST: DetailWorkers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DetailWorker detailWorker)
        {
            if (ModelState.IsValid)
            {
                db.DetailWorkers.Add(detailWorker);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(CombosHelper.Getcompanies(), "CompanyId", "Name", detailWorker.CompanyId);
            ViewBag.GraduateId = new SelectList(CombosHelper.GetGraduates(), "GraduateId", "GraduateName", detailWorker.GraduateId);
            ViewBag.JobTittleId = new SelectList(CombosHelper.GetJobTittles(), "JobTittleId", "JobTittleName", detailWorker.JobTittleId);
            return View(detailWorker);
        }
        [Authorize(Roles = "Admin")]
        // GET: DetailWorkers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetailWorker detailWorker = db.DetailWorkers.Find(id);
            if (detailWorker == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(CombosHelper.Getcompanies(), "CompanyId", "Name", detailWorker.CompanyId);
            ViewBag.GraduateId = new SelectList(CombosHelper.GetGraduates(), "GraduateId", "GraduateName", detailWorker.GraduateId);
            ViewBag.JobTittleId = new SelectList(CombosHelper.GetJobTittles(), "JobTittleId", "JobTittleName", detailWorker.JobTittleId);
            return View(detailWorker);
        }

        // POST: DetailWorkers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DetailWorker detailWorker)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detailWorker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(CombosHelper.Getcompanies(), "CompanyId", "Name", detailWorker.CompanyId);
            ViewBag.GraduateId = new SelectList(CombosHelper.GetGraduates(), "GraduateId", "GraduateName", detailWorker.GraduateId);
            ViewBag.JobTittleId = new SelectList(CombosHelper.GetJobTittles(), "JobTittleId", "JobTittleName", detailWorker.JobTittleId);
            return View(detailWorker);
        }
        [Authorize(Roles = "Admin")]
        // GET: DetailWorkers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetailWorker detailWorker = db.DetailWorkers.Find(id);
            if (detailWorker == null)
            {
                return HttpNotFound();
            }
            return View(detailWorker);
        }

        // POST: DetailWorkers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DetailWorker detailWorker = db.DetailWorkers.Find(id);
            db.DetailWorkers.Remove(detailWorker);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
