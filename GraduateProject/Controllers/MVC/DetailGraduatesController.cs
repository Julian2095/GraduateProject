﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GraduateProject.Models;
using GraduateProject.Classes;
using PagedList;

namespace GraduateProject.Controllers
{
    public class DetailGraduatesController : Controller
    {
        private GraduateProjectContext db = new GraduateProjectContext();

        // GET: DetailGraduates
        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            var detailGraduates = db.DetailGraduates.Include(d => d.EducationalStage).Include(d => d.Graduate).Include(d => d.Program).Include(d => d.University).
                OrderBy(g => g.Graduate.FirstName).ThenBy(g => g.University.UniversityName);
            return View(detailGraduates.ToPagedList((int)page, 5));
        }
        [Authorize(Roles = "Graduate, Admin")]
        // GET: DetailGraduates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetailGraduate detailGraduate = db.DetailGraduates.Find(id);
            if (detailGraduate == null)
            {
                return HttpNotFound();
            }
            return View(detailGraduate);
        }
        [Authorize(Roles = "Admin")]
        // GET: DetailGraduates/Create
        public ActionResult Create()
        {
            ViewBag.EducationalStageId = new SelectList(CombosHelper.GetEducationalStages(), "EducationalStageId", "Name");
            ViewBag.GraduateId = new SelectList(CombosHelper.GetGraduates(), "GraduateId", "GraduateName");
            ViewBag.ProgramId = new SelectList(CombosHelper.GetPrograms(), "ProgramId", "NameProgram");
            ViewBag.UniversityId = new SelectList(CombosHelper.GetUniversities(), "UniversityId", "UniversityName");
            return View();
        }

        // POST: DetailGraduates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DetailGraduate detailGraduate)
        {
            if (ModelState.IsValid)
            {
                db.DetailGraduates.Add(detailGraduate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EducationalStageId = new SelectList(CombosHelper.GetEducationalStages(), "EducationalStageId", "Name", detailGraduate.EducationalStageId);
            ViewBag.GraduateId = new SelectList(CombosHelper.GetGraduates(), "GraduateId", "GraduateName", detailGraduate.GraduateId);
            ViewBag.ProgramId = new SelectList(CombosHelper.GetPrograms(), "ProgramId", "NameProgram", detailGraduate.ProgramId);
            ViewBag.UniversityId = new SelectList(CombosHelper.GetUniversities(), "UniversityId", "UniversityName", detailGraduate.UniversityId);
            return View(detailGraduate);
        }
        [Authorize(Roles = "Admin")]
        // GET: DetailGraduates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetailGraduate detailGraduate = db.DetailGraduates.Find(id);
            if (detailGraduate == null)
            {
                return HttpNotFound();
            }
            ViewBag.EducationalStageId = new SelectList(CombosHelper.GetEducationalStages(), "EducationalStageId", "Name", detailGraduate.EducationalStageId);
            ViewBag.GraduateId = new SelectList(CombosHelper.GetGraduates(), "GraduateId", "GraduateName", detailGraduate.GraduateId);
            ViewBag.ProgramId = new SelectList(CombosHelper.GetPrograms(), "ProgramId", "NameProgram", detailGraduate.ProgramId);
            ViewBag.UniversityId = new SelectList(CombosHelper.GetUniversities(), "UniversityId", "UniversityName", detailGraduate.UniversityId);
            return View(detailGraduate);
        }

        // POST: DetailGraduates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DetailGraduate detailGraduate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detailGraduate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EducationalStageId = new SelectList(CombosHelper.GetEducationalStages(), "EducationalStageId", "Name", detailGraduate.EducationalStageId);
            ViewBag.GraduateId = new SelectList(CombosHelper.GetGraduates(), "GraduateId", "GraduateName", detailGraduate.GraduateId);
            ViewBag.ProgramId = new SelectList(CombosHelper.GetPrograms(), "ProgramId", "NameProgram", detailGraduate.ProgramId);
            ViewBag.UniversityId = new SelectList(CombosHelper.GetUniversities(), "UniversityId", "UniversityName", detailGraduate.UniversityId);
            return View(detailGraduate);
        }
        [Authorize(Roles = "Admin")]
        // GET: DetailGraduates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetailGraduate detailGraduate = db.DetailGraduates.Find(id);
            if (detailGraduate == null)
            {
                return HttpNotFound();
            }
            return View(detailGraduate);
        }

        // POST: DetailGraduates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DetailGraduate detailGraduate = db.DetailGraduates.Find(id);
            db.DetailGraduates.Remove(detailGraduate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
