﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GraduateProject.Startup))]
namespace GraduateProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
