﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GraduateProject.Models
{
    public class City
    {
        [Key]
        public int CityId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(40, ErrorMessage = "The field {0} must be shorter than 40 characters length")]
        [Display(Name = "City")]
        
        [Index("City_Name_Index",IsUnique = true)]
        public string Name { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
       
        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Graduate> Graduates { get; set; }
        public virtual ICollection<University> Universitites { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<LaborSupply> LaborSupplies { get; set; }
    }
}