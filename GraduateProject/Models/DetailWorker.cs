﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GraduateProject.Models
{
    public class DetailWorker
    {
        [Key]
        public int DetailWorkerId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "Graduate")]
        public int GraduateId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "Company")]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "Job tittle")]
        public int JobTittleId { get; set; }

        public virtual Graduate Graduate { get; set; }
        public virtual JobTittle JobTittle { get; set; }
        public virtual Company Company { get; set; }
        
    }
}