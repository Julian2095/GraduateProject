﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GraduateProject.Models
{
    public class University
    {
        [Key]
        public int UniversityId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(150, ErrorMessage = "The field {0} must be shorter than 40 characters length")]
        [Display(Name = "University")]
        [Index("University_UniversityName_Index", IsUnique = true)]
        public string UniversityName { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(20, ErrorMessage = "The field {0} must be shorter than 40 characters length")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(1000, ErrorMessage = "The field {0} must be shorter than 40 characters length")]
        public string Adress { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Photo { get; set; }

        [NotMapped]
        public HttpPostedFileBase PhotoFile { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "Department")]
        public int DepartmentId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "City")]
        public int CityId { get; set; }

        public virtual Department Department { get; set; }

        public virtual City City { get; set; }

        public virtual ICollection<Event> Events { get; set; }

        public virtual ICollection<DetailGraduate> DetailGraduates { get; set; }
    }
}
