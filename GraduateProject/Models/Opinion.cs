﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GraduateProject.Models
{
    public class Opinion
    {
        [Key]
        public int Opiniond { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(50, ErrorMessage = "The field {0} must be shorter than 40 characters length")]
        [Display(Name = "Opinion Name")]
        public string OpinionName { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(1000, ErrorMessage = "The field {0} must be shorter than 500 characters length")]
        [Display(Name = "Your opion")]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "Graduate")]
        public int GraduateId { get; set; }

        public virtual Graduate Graduate { get; set; }
    }
}