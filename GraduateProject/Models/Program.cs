﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GraduateProject.Models
{
    public class Program
    {
        [Key]
        public int ProgramId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(40, ErrorMessage = "The field {0} must be shorter than 40 characters length")]
        [Display(Name = "Programs")]
        [Index("Program_NameProgram_Index", IsUnique = true)]
        public string NameProgram { get; set; }

        public virtual ICollection<DetailGraduate> DetailGraduates { get; set; }
    }
}