﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GraduateProject.Models
{
    public class JobTittle
    {
        [Key]
        public int JobTittleId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(60, ErrorMessage = "The field {0} must be shorter than 60 characters length")]
        [Display(Name = "Job tittle")]
        //[Index("City_Name_Index", 2 ,IsUnique = true)]
        [Index("JobTittle_Name_Index", IsUnique = true)]
        public string JobTittleName { get; set; }

        public virtual ICollection<DetailWorker> DetailWorkers { get; set; }
    }
}