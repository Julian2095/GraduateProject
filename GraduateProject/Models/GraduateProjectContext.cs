﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace GraduateProject.Models
{
    public class GraduateProjectContext : DbContext
    {
        //Para crear la conexión a la base de datos
        public GraduateProjectContext() : base("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }


        public System.Data.Entity.DbSet<GraduateProject.Models.Department> Departments { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.City> Cities { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.Company> Companies { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.Graduate> Graduates { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.EducationalStage> EducationalStages { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.University> Universities { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.Event> Events { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.Program> Programs { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.DetailGraduate> DetailGraduates { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.JobTittle> JobTittles { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.DetailWorker> DetailWorkers { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.LaborSupply> LaborSupplies { get; set; }

        public System.Data.Entity.DbSet<GraduateProject.Models.Opinion> Opinions { get; set; }

        
    }
}