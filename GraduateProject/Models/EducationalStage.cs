﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace GraduateProject.Models
{
    public class EducationalStage
    {
        [Key]
        public int EducationalStageId { get; set; }

        [Required (ErrorMessage = "This field is required")]
        [Display (Name = "Educational Stage")]
        public String Name { get; set; }

        public virtual ICollection<DetailGraduate> DetailGraduates { get; set; }
    }
}