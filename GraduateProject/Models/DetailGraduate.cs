﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GraduateProject.Models
{
    public class DetailGraduate
    {
        [Key]
        public int DetailGraduateId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "Graduate")]
        public int GraduateId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "Program")]
        public int ProgramId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "University")]
        public int UniversityId { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "You must select a {0}")]
        [Display(Name = "Educational Stage")]
        public int EducationalStageId { get; set; }

        public virtual Graduate Graduate { get; set; }
        public virtual Program Program { get; set; }
        public virtual University  University { get; set; }
        public virtual EducationalStage EducationalStage { get; set; }
    }
}