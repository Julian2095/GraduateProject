﻿using GraduateProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraduateProject.Classes
{
    public class GraduateResponse
    {
        public int GraduateId { get; set; }

        public string GraduateName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Photo { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public int CityId { get; set; }

        public string CityName { get; set; }

    }
}