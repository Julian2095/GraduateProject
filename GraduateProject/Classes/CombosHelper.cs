﻿using GraduateProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraduateProject.Classes
{
    public class CombosHelper : IDisposable
    {
        private static GraduateProjectContext db = new GraduateProjectContext();

        public static List<Department> GetDepartments()
        {
            var departments = db.Departments.ToList();
            departments.Add(new Department
            {
                DepartmentId = 0,
                Name = "[Select a department]",
            });
            departments = departments.OrderBy(o => o.Name).ToList();

            return departments.OrderBy(o => o.Name).ToList();
        }

        public static List<City> GetCities()
        {
            var Cities = db.Cities.ToList();
            Cities.Add(new City
            {
                CityId = 0,
                Name = "[Select a City]",
            });
           
            return Cities.OrderBy(o => o.Name).ToList();
        }

        public static List<Company> Getcompanies()
        {
            var Companies = db.Companies.ToList();
            Companies.Add(new Company
            {
                CompanyId = 0,
                Name = "[Select a Company]",
            });

            return Companies.OrderBy(o => o.Name).ToList();
        }

        public static List<University> GetUniversities()
        {
            var Universities = db.Universities.ToList();
            Universities.Add(new University
            {
                UniversityId = 0,
                UniversityName = "[Select a Universit]",
            });

            return Universities.OrderBy(o => o.UniversityName).ToList();
        }


        public static List<EducationalStage> GetEducationalStages()
        {
            var EducationalStage = db.EducationalStages.ToList();
            EducationalStage.Add(new EducationalStage
            {
                EducationalStageId = 0,
                Name = "[Select a EducationalStage]",
            });

            return EducationalStage.OrderBy(o => o.Name).ToList();
        }

        public static List<Program> GetPrograms()
        {
            var Program = db.Programs.ToList();
            Program.Add(new Program
            {
                ProgramId = 0,
                NameProgram = "[Select a Program]",
            });

            return Program.OrderBy(o => o.NameProgram).ToList();
        }

        public static List<Graduate> GetGraduates()
        {
            var Graduate = db.Graduates.ToList();
            Graduate.Add(new Graduate
            {
               GraduateId = 0,
                GraduateName = "[Select a graduate's adress]",
            });

            return Graduate.OrderBy(o => o.Adress).ToList();
        }

        public static List<JobTittle> GetJobTittles()
        {
            var JobTittle = db.JobTittles.ToList();
            JobTittle.Add(new JobTittle
            {
                JobTittleId = 0,
                JobTittleName = "[Select a Job tittle]",
            });

            return JobTittle.OrderBy(o => o.JobTittleName).ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}